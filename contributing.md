# Contributing

For minor fixes or new templates, fork this repository and submit a pull request.

For issues related to the content of the kata, please open an issue to discuss.
