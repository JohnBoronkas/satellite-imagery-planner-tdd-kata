# Part 1

Throughout this kata you are a part of a fictional team that is working on a ground system that will produce plans of how to allocate satellites to collect images of the Earth's surface. The team has a rough design for the system and some documentation to work off of.

You will be working alone on the planning service. As the system matures, additional requirements will be added and other teams will be independently maturing the services that the planning service needs to interact with.

## Planning service

### Overview

The planning service will be sent JSON that details satellites in various orbits around the Earth. Each satellite knows which satellites it can communicate with, which requested image locations it can take an image of, and if it has a groundlink.

Included in that JSON is a list of image locations that need to be captured.

Each satellite can only take one image per plan created due to power limiations on the satellite.

Once the satellite has the image, it needs to send the image to a groundlink. If it can't send the image directly itself due to not having a groundlink, then it needs to send the image to another satellite that is connected to a groundlink.

The service needs to respond, in JSON, with each image location it can satisfy with the ordered links that the image needs to take to make it to the groundlink. The first satellite listed is the satellite that is taking the image, the last satellite listed is the satellite that will send it to the groundlink.
- A list with only one satellite listed means that the satellite taking the image is also uploading it
- An empty list means that there are no images that can be collected

> You don't actually need to build a REST interface, message queue, or anything like that. It can just be a simple function that takes the JSON as an argument. The interface that backs this function is implied (unless you want to go above and beyond).

> Example:

```python
def make_plan(json_input):
    pass
```

#### Example scenario

![Problem 1 example scenario](img/problem_01.png)

In the above scenario, satellite "A" has a groundlink and can communicate with satellite "B". Satellite "B" can see a requested image location and can communicate with satellite "A", but it does not have a groundlink.

To solve it, satellite "B" needs to take the image, then send it to satellite "A" to be sent to the groundlink.

#### Input

```json
{
    "satellites": [
        {
            "id": 101,
            "hasGroundlink": true,
            "satelliteLinks": [102],
            "imageLocations": []
        },
        {
            "id": 102,
            "hasGroundlink": false,
            "satelliteLinks": [101],
            "imageLocations": [201]
        }
    ],
    "requestedImageLocations": [201]
}
```

#### Output

```json
{
    "satisfiedImages": [
        {
            "location": 201,
            "linkPath": [102, 101]
        }
    ]
}
```

## Getting started

If you're feeling confident, then jump right in! If you want a little more structure in your life to get you going or if you get stuck, use the `part_01/guide.md` file!

When you are finished, read up to and through chapter `X`, stopping at the start of chapter `X`. Then open `part_02/readme.md` to continue!

`TODO Next part`
