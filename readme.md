# Satellite Imagery Planner - TDD Kata

Follow along by reading "Test-Driven Development by Example" written by Kent Beck.

Read the relevant section of the book, then open the matching directory for the next step of the program that will help enforce what you just read.

- Don't skip ahead! Only go into the directories when directed to get the most out of this TDD Kata.
- I also recommend following along with the book in your editor to help get you used to the new workflow to begin building the habit.

Use the language you are most comfortable with and submit an issue or pull request if something isn't clear!

## Target audience

It is expected that you know your langague and a unit testing framework for that language.

## Getting started

Read up to and through chapter 4, stopping at the start of chapter 5. Then open `part_01/readme.md` to get started! 🎉

### TDD Templates

Templates to get you started in the language of your choice faster are available in `templates`.
