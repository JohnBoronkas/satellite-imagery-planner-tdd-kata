import json


def make_plan(request):
    return request


if __name__ == "__main__":
    with open("request.json") as request:
        response = make_plan(json.load(request))
    
    print(json.dumps(response, indent=4))
    
